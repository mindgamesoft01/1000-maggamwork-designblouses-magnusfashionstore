package com.magnus.maggumwork.designs.collection2019

interface AppInterfaces {
    fun loadItem()
    fun loadSplashScreen()
    fun loadStartScreen()
    fun loadImageTopics()
    fun loadMenus()
    fun loadTestModeScreen()
    fun loadBookMarkMenu()
    fun loadBookMarkItem()
    fun loadPrivacyPolicy()


}