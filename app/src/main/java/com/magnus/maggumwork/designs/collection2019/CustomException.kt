package com.magnus.maggumwork.designs.collection2019

class CustomException(message: String) : Exception(message)
